<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use App\Service\BlogPostService;
use App\Entity\BlogPost;

class ApiTest extends WebTestCase
{
    public function testApiCall(): void
    {
        $client = static::createClient();

        $container = static::getContainer();

        $blogPostServiceMock = $this->createMock(BlogPostService::class);

        $blogPostPublished = new BlogPost();
        $blogPostPublished->setId(1);
        $blogPostPublished->setTitle('test insert');
        $blogPostPublished->setContent('test insert content');
        $blogPostPublished->setStatus(BlogPost::PUBLISHED);

        $blogPosts = [
            $blogPostPublished,
        ];

        $blogPostServiceMock->expects($this->any())
            ->method('getAll')
            ->willReturn($blogPosts);

        $this->getContainer()->set(BlogPostService::class, $blogPostServiceMock);

        $crawler = $client->request('GET', '/to/be/tested');

        $this->assertResponseIsSuccessful();

        $response = $client->getResponse();

        $responseData = json_decode($response->getContent(), true);

        $this->assertEquals(1, count($responseData));

        $this->assertEquals("Titel: " . $blogPostPublished->getTitle(), $responseData[0]['title']);
    }
}
