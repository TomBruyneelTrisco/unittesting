<?php

namespace App\Tests;

use Symfony\Component\Form\Test\TypeTestCase;
use App\Form\Type\BlogPostType;
use App\Entity\BlogPost;

// for more info: https://symfony.com/doc/current/form/unit_testing.html

class BlogPostTypeTest extends TypeTestCase
{
    public function testSubmitValidData()
    {
        $formData = [
            'title' => 'titel blogpost',
            'content' => 'content van de blogpost',
            'status' => 'published',
        ];

        $model = new BlogPost();
        
        $form = $this->factory->create(BlogPostType::class, $model);

        $expected = new BlogPost();
        $expected->setTitle('titel blogpost');
        $expected->setContent('content van de blogpost');
        $expected->setStatus(BlogPost::PUBLISHED);
        
        $form->submit($formData);

        // check if form validation works
        $this->assertTrue($form->isValid());

        // check for transformation failures
        $this->assertTrue($form->isSynchronized());

        // was the transformation correct
        $this->assertEquals($expected, $model);
    }
}
