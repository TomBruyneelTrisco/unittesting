<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use App\Service\BlogPostService;
use App\Entity\BlogPost;

class BlogPostServiceTest extends KernelTestCase
{
    public function testBlogPostService(): void
    {
        $kernel = self::bootKernel();

        $container = static::getContainer();

        $blogPostService = $container->get(BlogPostService::class);

        $this->assertSame('test', $kernel->getEnvironment());
        
        $blogPostPublished = new BlogPost();
        $blogPostPublished->setTitle('test insert');
        $blogPostPublished->setContent('test insert content');
        $blogPostPublished->setStatus(BlogPost::PUBLISHED);

        $blogPostPinned = new BlogPost();
        $blogPostPinned->setTitle('test insert pinned');
        $blogPostPinned->setContent('test insert pinned content');
        $blogPostPinned->setStatus(BlogPost::PINNED);

        $blogPostService->insertBlogPost($blogPostPinned);
        $blogPostService->insertBlogPost($blogPostPublished);

        $allBlogPosts = $blogPostService->getAll();
        $publishedBlogPosts = $blogPostService->getAllPublished();

        $this->assertEquals(1, count($publishedBlogPosts), "There should only be 1 published blog post");
        $this->assertEquals(2, count($allBlogPosts));

        $this->assertSame($publishedBlogPosts[0], $blogPostPublished);
    }
}
