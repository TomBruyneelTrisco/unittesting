<?php

namespace App\Tests;

use PHPUnit\Framework\TestCase;
use App\Helpers\Transformers;
use App\Entity\BlogPost;

// Simple unit tests that don't need services should extend TestCase, write as many of these as you want, the more the merrier

class TransformerFunctionsTest extends TestCase
{
    public function testStatusStringToInteger(): void
    {
        // we also have to support the old cases
        $string_status = "publish";

        $this->assertTrue(Transformers::convertStatusToInt($string_status) == BlogPost::PUBLISHED, "The transformation of the status string to integer failed");
    }
}
