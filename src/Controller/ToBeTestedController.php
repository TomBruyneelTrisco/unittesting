<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\BlogPostRepository;
use App\Service\BlogPostService;
use Symfony\Component\HttpFoundation\Request;

class ToBeTestedController extends AbstractController
{
    /**
     * @Route("/to/be/tested", name="to_be_tested")
     */
    public function index(BlogPostService $blogPostService, Request $request): Response
    {
        if ($request->query->get('onlyPublished')) {
            $blogPosts = $blogPostService->getAllPublished();
        } else {            
            $blogPosts = $blogPostService->getAll();
        }

        return $this->json($blogPosts);
    }

    /**
     * @Route("/to/be/tested/{id}", name="blog_show")
     */
    public function blog_post(int $id, BlogPostRepository $blogPostRepository): Response
    {
        $blogPost = $blogPostRepository->find($id);
        return $this->json($blogPost);
    }
}
