<?php

namespace App\Controller;

use App\Form\Type\BogPostStatusType;
use App\Form\Type\BlogPostType;
use App\Entity\BlogPost;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\BlogPostService;

class BlogPostController extends AbstractController
{
    /**
     * @Route("/blogpost/new", name="blogpost_new")
     */
    public function new(Request $request, BlogPostService $blogPostService): Response
    {
        // creates a task object and initializes some data for this example
        $blogPost = new BlogPost();

        $form = $this->createForm(BlogPostType::class, $blogPost);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $newBlogPost = $form->getData();

            $blogPostService->insertBlogPost($newBlogPost);

            return $this->redirectToRoute('to_be_tested');
        }

        return $this->renderForm('blogpost/new.html.twig', [
            'form' => $form,
        ]);
    }
}