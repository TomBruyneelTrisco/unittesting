<?php

namespace App\Serializer;

use App\Entity\BlogPost;
use App\Helpers\Transformers;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Serializer\Normalizer\ContextAwareNormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class BlogPostSerializer implements ContextAwareNormalizerInterface
{
    private $router;
    private $normalizer;

    public function __construct(UrlGeneratorInterface $router, ObjectNormalizer $normalizer)
    {
        $this->router = $router;
        $this->normalizer = $normalizer;
    }

    public function normalize($blogPost, string $format = null, array $context = [])
    {
        $data = $this->normalizer->normalize($blogPost, $format, $context);

        $data['id'] = $this->router->generate('blog_show', [
            'id' => $blogPost->getId(),
        ], UrlGeneratorInterface::ABSOLUTE_URL);

        $data['status'] = Transformers::convertIntToStatus($blogPost->getStatus());
        $data['title'] = "Titel: " . $blogPost->getTitle();

        return $data;
    }

    /**
     * @return bool
     */
    public function supportsNormalization($data, string $format = null, array $context = [])
    {
        return $data instanceof BlogPost;
    }
}