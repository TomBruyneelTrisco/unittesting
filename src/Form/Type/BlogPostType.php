<?php

namespace App\Form\Type;

use App\Form\Type\BogPostStatusType;
use App\Helpers\Transformers;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use App\Entity\BlogPost;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class BlogPostType extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => BlogPost::class,
        ]);
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('title', TextType::class)
            ->add('content', TextType::class)
            ->add('status', BlogPostStatusType::class)
            ->add('save', SubmitType::class, ['label' => 'Create Blogpost']);

        $builder
            ->get('status')
            ->addModelTransformer(new CallbackTransformer(
                function ($statusAsInt) {
                    switch ($statusAsInt) {
                        case BlogPost::PINNED:
                            return 'pinned';
                        case BlogPost::PUBLISHED:
                            return 'published';
                        case BlogPost::UNPUBLISHED:
                            return 'unpublished';
                    }
                },
                function ($statusAsString) {
                    return Transformers::convertStatusToInt($statusAsString);
                }
            ));
    }
}