<?php

namespace App\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\OptionsResolver\OptionsResolver;


class BlogPostStatusType extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'choices' => [
                'Pinned' => 'pinned',
                'Published' => 'published',
                'Unpublished' => 'unpublished',
            ],
        ]);
    }

    public function getParent(): string
    {
        return ChoiceType::class;
    }
}