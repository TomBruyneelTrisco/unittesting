<?php

namespace App\Helpers;

use App\Entity\BlogPost;

class Transformers {

    public static function convertStatusToInt(string $statusAsString) {
        switch ($statusAsString) {
            case 'pinned':
            case 'Pinned':
                return BlogPost::PINNED;
            case 'publish':
            case 'published':
                return BlogPost::PUBLISHED;
            case 'unpublished':
                return BlogPost::UNPUBLISHED;
        }

        return 0;
    }

    public static function convertIntToStatus(string $statusAsInt) {
        switch ($statusAsInt) {
            case BlogPost::PINNED:
                return 'pinned';
            case BlogPost::PUBLISHED:
                return 'published';
            case BlogPost::UNPUBLISHED:
                return 'unpublished';
        }

        return 'unknown status';
    }
}