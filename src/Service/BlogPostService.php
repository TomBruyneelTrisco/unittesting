<?php

namespace App\Service;

use App\Entity\BlogPost;
use Doctrine\Persistence\ManagerRegistry;
use App\Repository\BlogPostRepository;

class BlogPostService {
    private $doctrine;
    private $blogPostRepository;

    public function __construct(ManagerRegistry $doctrine, BlogPostRepository $blogPostRepository)
    {
        $this->doctrine = $doctrine;
        $this->blogPostRepository = $blogPostRepository;
    }
    
    function insertBlogPost(BlogPost $blogPost) : void {
        $entityManager = $this->doctrine->getManager();

        $entityManager->persist($blogPost);
        $entityManager->flush();
    }

    /**
     * @return BlogPost[]
     */
    function getAllPublished(): iterable {
        return $this->blogPostRepository->findBy(array('status' => BlogPost::PUBLISHED));
    }

    /**
     * @return BlogPost[]
     */
    function getAll(): iterable {
        return $this->blogPostRepository->findAll();
    }
}
